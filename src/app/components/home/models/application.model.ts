import { Study } from './studies.model';

export class Application {
    applicationNumber: string;
    sponsorName: string;
    studies: Study[];
    expanded: boolean;
}
