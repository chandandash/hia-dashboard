import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class HomeService {

    constructor(private http: HttpClient) { }

    public getApplications() {
        return this.http.get("./assets/json/applications.json");
    }

}