export class Study {
    studyKey: string;
    analysisStudyKey: string;
    studyID: string;
    studyName: string;
    studyType: string;
    studyTitle: string;
    keywords: string;
    studyLoadDate: string;
}
