import { Component, OnInit } from '@angular/core';
import { HttpClient } from 'selenium-webdriver/http';
import { HomeService } from './models/home.service';
import { Application } from './models/application.model';


@Component({
  selector: 'home-component',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  constructor(private homeservice: HomeService) { }

  applications: Application[];

  ngOnInit() {
    this.getApplicationData();
  }

  getApplicationData() {
    this.homeservice.getApplications()
      .subscribe((applications: Application[]) => this.applications = applications);
  }


}

