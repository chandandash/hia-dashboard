import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  @Output() showSideNav = new EventEmitter<boolean>();

  isShow: boolean;

  @Input()
  public set closeNav(show: boolean) {
    this.isShow = show;
  }
  constructor() {

  }

  ngOnInit() {
  }

  toggleSideNav() {
    this.isShow = !this.isShow;
    this.showSideNav.emit();
  }
}
