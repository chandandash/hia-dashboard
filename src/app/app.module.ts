import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SidepanelService } from './services/sidepanel.service';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule } from '@angular/material';
import { MaterialModule } from './material-module/material.module';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { HomeComponent } from './components/home/home.component';
import { SummaryComponent } from './components/summary/summary.component';
import { SidepanelComponent } from './components/sidepanel/sidepanel.component';
import { AgGridModule } from 'ag-grid-angular';
import { HttpClientModule } from '@angular/common/http';
import { HomeService } from './components/home/models/home.service';


@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    HomeComponent,
    SummaryComponent,
    SidepanelComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MaterialModule,
    HttpClientModule,
    FlexLayoutModule,
    AgGridModule.withComponents([])
  ],
  providers: [
    SidepanelService,
    HomeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
